<?php

namespace Framework\Routing\Matching;


interface Validator
{
    public function match($request, $route);
}