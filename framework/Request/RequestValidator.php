<?php

namespace Framework\Request;

interface RequestValidator {

    public function handle();

}