<?php

namespace Framework\Routing;

use Framework\Routing\Route;
use Framework\Routing\Matching\CheckMatches;

class Router {

    protected static $routes_list = [];

    public static function get($route, $action)
    {
        self::addToRoutesList($route, $action, 'GET');
    }

    public static function post($route, $action)
    {
        self::addToRoutesList($route, $action, 'POST');
    }

    public static function delete($route, $action)
    {
        self::addToRoutesList($route, $action, 'DELETE');
    }

    public static function put($route, $action)
    {
        self::addToRoutesList($route, $action, 'PUT');
    }

   static public function getRoutesList()
    {
        return self::$routes_list;
    }

    static public function addToRoutesList($url, $action, $method){
        array_push(self::$routes_list, new Route($url, $action, $method)/*$route_info*/);
    }


    static function match($request)
    {
        foreach (self::$routes_list as $route) {
            if ($route->check($request)) {
                return $route;
            }
        }

        return false;
    }

}