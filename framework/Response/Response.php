<?php

namespace Framework\Response;
use Framework\View\View;


class Response {

    private static $_instance = null;

    public $headers         = [];
    public $response_data   = [];

    private function __construct() {}

    protected function __clone() {}

    static public function getInstance()
    {
        if(is_null(self::$_instance))
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    public function makeResponse()
    {
        return $this;
    }

    public function response($inputs = null, $status = null)
    {
        $this->response_data = $inputs;
        $this->status  = $status;
        return $this;
    }

    public function json($data, $status = null)
    {
        $this->response_data = json_encode($data);
        $this->status  = $status;
        return $this;
    }

    public function header($key, $value)
    {
        $this->headers[$key] = $value;
        return $this;
    }

    public function withHeaders($inputs)
    {
        array_merge($this->headers, $inputs);
        return $this;
    }

    public function redirect($url)
    {
        $this->redirect = $url;
        return $this;
    }

    public function with($inputs)
    {
        $this->response_data = $inputs;
        return $this;
    }

    public function view($name)
    {
        $view = new View();
        $this->view = $view->view($name);
        $this->layout = dirname(__FILE__) . "/../../resources/Views/layouts/layout.php";
        
        return $this;
    }
    
    public function render()
    {
        if(isset($this->view)) {
            ob_start();
            extract($this->response_data);
            include($this->layout);
            $content = ob_get_contents();
            ob_end_clean();
            echo($content);
        }else {
            echo($this->response_data);
        }

        return $this;
    }
}