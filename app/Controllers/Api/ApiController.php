<?php

namespace App\Controllers\Api;

use Framework\Controller\BaseController;
use App\Models\User;

class ApiController extends BaseController{

    public function __construct()
    {
        $this->middleware(['check_api']);
        $this->user = new User();
    }

    public function index($id)
    {
        $user = $this->user->find($id);
        return response()->json(['user' => $user], 200);
    }
}