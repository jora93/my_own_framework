<?php
/**
 * Created by PhpStorm.
 * User: Syuniq
 * Date: 01-Mar-18
 * Time: 5:03 PM
 */

namespace Framework\Routing\Matching;


class UrlMatch implements Validator
{
    public function match($request, $route)
    {
        return preg_match($route->getRegex(), $request->uri());
    }
}