<?php

namespace App\Middleware;

use Framework\Middleware\Middleware;
use Framework\Request\Request;

class CheckWeb implements Middleware {

    public function handle()
    {
        $request = new Request();
        $headers = $request->getAllHeaders();
        if (!isset($headers['FRAMEWORK']) && (substr($request->uri(), 0, 5 !== '/api/'))) {
            return true;
        }
        return redirect('google.com');
    }
}