<?php

namespace Framework\Routing;

use Framework\Routing\Matching\CheckMatches;
use Framework\Routing\Matching\UrlMatch;
use App\Kernel;
use Framework\AppKernel;

class Route
{
    private static $validators;

    protected static $route_data = [];
    protected static $route = '';
    protected static $namespace = '';

    public function __construct($url, $action, $method)
    {
//        $obj = new self($url, $action, $method);
        $this->url = $url;
        $this->method = $method;
        $this->regex = $this->getRegex($url);
        $action_parts = explode("@", $action);
        $this->controller = $action_parts[0];
        $this->action = $action_parts[1];
    }

    public function getRegex()
    {
        $route_parts = explode("/", $this->url);
        $regex = '/';

        foreach ($route_parts as $part) {
            if ($part == '') {
                $regex .= '^';
            } elseif (preg_match('/\{\$(.*?)\}/', $part, $matches)) {
                if ($part == $route_parts[count($route_parts) - 1]) {
                    $regex .= '\/([^\/]+)';
                } else {
                    $regex .= '\/(.+)';
                }
            } else {
                $regex .= '\/' . $part;
            }
        }

        $regex .= '$/';

        if ($this->url == '/') {
            $regex = '/^\/(?!.)/';
        }

        return $regex;
    }

//    public function middleware($middleware_group)
//    {
//        Kernel::runMiddlewareGroups($middleware_group);
//        return $this;
//    }

    public function namespace($namespace)
    {
        self::$namespace = $namespace;
        return $this;
    }

    public function route($route)
    {
        self::$route = $route;
    }

    public static function getRoute()
    {
        return self::$route;
    }

    public static function getNamespace()
    {
        return self::$namespace;
    }

    public static function getValidators()
    {
        if (isset(self::$validators)) {
            return self::$validators;
        }

        return self::$validators = [
            new UrlMatch()
        ];
    }

    public function check($request)
    {
        foreach (self::getValidators() as $validator) {
            if (!$validator->match($request, $this)) {
                return false;
            }
        }

        return true;
    }
}