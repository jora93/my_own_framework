<?php
namespace Framework\Middleware;

Interface Middleware {

    public function handle();

}