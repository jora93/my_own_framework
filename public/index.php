<!--<script src="./index.js"></script>-->
<?php

use Framework\Application;

spl_autoload_register(function ($class) {
    include dirname(__FILE__) .'\..\\'.strtolower (substr($class, 0,1)).substr($class, 1).'.php';
});

$app = Application::getInstance();

$app->run();
