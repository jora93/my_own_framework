<?php

namespace Framework\View;

class View{

    private $view_path;

    public function __construct()
    {
    }

    public function view($name)
    {
        $file_path = dirname(__FILE__) . "/../../resources/Views";
        $path_parts = explode(".", $name);
        foreach ($path_parts as $path_part) {
            $file_path .= "/".$path_part;
        }
        $file_path = $file_path.'.php';
        return $file_path;
    }

    public function with($params)
    {
        $params['view_path'] = $this->view_path;
        extract($params);
        require ( dirname(__FILE__) . "/../../resources/Views/layouts/layout.php");
    }
}