<?php

use Framework\Routing\Router;

Router::get('/', 'HomeController@index');
Router::get('/user', 'UserController@index');
Router::get('/user/{$id}', 'UserController@getUser');
Router::get('/user/{$id}/order/{$name}', 'HomeController@index');
Router::post('/store', 'HomeController@store');