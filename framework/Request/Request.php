<?php

namespace Framework\Request;

class Request {

    public function method()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    public function uri()
    {
        return  rawurldecode( $_SERVER['REQUEST_URI']);
    }

    public function getAllHeaders()
    {
        $headers = [];
        foreach ($_SERVER as $name => $value)
        {
            if (substr($name, 0, 5) == 'HTTP_')
            {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }

    public function makeRequest($route)
    {
        $this->controller = '\App\Controllers\\'.str_replace("/", '\\', $route->controller);
        $this->action = $route->action;
        preg_match($route->regex, $this->uri(), $matches);
        unset($matches[0]);
        $this->params = $matches;
        return $this;
    }

}