<?php

namespace App\Middleware;

use Framework\Middleware\Middleware;
use Framework\Request\Request;
use Framework\Response\Response;

class CheckApi implements Middleware {

    public function handle()
    {
        $request = new Request();
        $headers = $request->getAllHeaders();
        if ( isset($headers['Framework'])  && (substr($request->uri(), 0, 5) == '/api/') ) {
            return true;
        }
        return redirect('google.com');
    }
}