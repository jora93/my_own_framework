"use strict";
var webpack = require("webpack");

module.exports = {
    // entry: './resources/js/app',
    context: __dirname + '/resources/js',
    entry: {
        app: './app',
        home: './home',
    },
    output: {
        path: __dirname + '/public/js',
        publicPath: '/js/',
        filename: '[name].js',
        // filename: '[name].js',
        library: '[name]',
    },
    watch: true,
    watchOptions: {
        aggregateTimeout: 100
    },
    devtool: 'inline-source-map',

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        })
    ]
};