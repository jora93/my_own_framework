<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>framework</title>
    <script src="/js/home.js"></script>
    <script src="/js/app.js"></script>
</head>
<body>

    <?php include_once 'header.php'; ?>

    <?php include_once $this->view;?>

    <?php include_once 'footer.php'; ?>
</body>

</html>