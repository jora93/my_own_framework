<?php

use Framework\View\View;
use Framework\Response\Response;


function env($key)
{
    $env = parse_ini_file("../env.ini");
    return $env[$key];
}

function requireFile($path)
{
    require $path;
}

function view($path)
{
    // $view = new View;
    // return $view->view($path);
    return Response::getInstance()->view($path);
    
}

function base_path($path)
{
    return dirname(__FILE__) . "/../../".$path;
}

function response($inputs = null, $status = null)
{
    return Response::getInstance()->response($inputs, $status);
}

function redirect($url)
{
    return Response::getInstance()->redirect($url);
}
