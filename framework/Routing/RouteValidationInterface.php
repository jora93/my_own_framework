<?php

namespace Framework\Routing;

interface RouteValidationInterface
{
    public function validateRequest();
}