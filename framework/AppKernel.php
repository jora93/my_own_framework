<?php

namespace Framework;


class AppKernel
{
    public function runRequestValidators()
    {
        if (!empty($this->request_validators)) {
            foreach ($this->request_validators as $request_validator) {
                $validator_class = new $request_validator;
                $validator_class->handle();
            }
        }
    }

    public function requireRoutes()
    {
        if (!empty($this->routes)) {
            foreach ($this->routes as $route) {
                require base_path($route);
            }
        }
    }

    public function getMiddleware($name)
    {
        if (isset($this->middleware[$name])) {
            return $this->middleware[$name];
        }
    }
}