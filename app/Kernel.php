<?php

namespace App;

use Framework\AppKernel;


class Kernel extends AppKernel{

    public static $providers = [
        \App\Providers\RouteServiceProvider::class,
    ];

    protected $middleware = [
        'check_web' => \App\Middleware\CheckWeb::class,
        'check_api' => \APP\Middleware\CheckApi::class,
    ];

    protected $routes = [
        'routes/route.php',
        'routes/api.php'
    ];

    protected $request_validators = [
        \App\Request\RequestTypeValidator::class,
    ];

    public static function runProviders() {
        foreach (self::$providers as $provider_class) {
            $provider = new $provider_class;
            $provider->map();
        }
    }

//    public static function runMiddlewareGroups($group) {
//        foreach (self::$middleware_groups[$group] as $middleware_class) {
//            $middleware = new $middleware_class;
//            $middleware->handle();
//        }
//    }
}
