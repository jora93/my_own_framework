<?php

namespace Framework\Routing\Matching;

use Framework\Routing\Router;
use Framework\Routing\Route;
use Framework\Request\Request;
use Framework\Routing\RouteValidationInterface;

class CheckMatches implements RouteValidationInterface {

    public function validateRequest()
    {
        $request    = new Request();
        $routes     = Router::getRoutesList();
        $uri        = $request->uri();
        $method     = $request->method();

        foreach ($routes as $route) {
            preg_match( $route->regex, $uri, $matches);
            if (!empty($matches) && ($method == $route->method)) {
                unset($matches[0]);
                $route->params =  $matches;
                return $route;
            }
        }
        return false;
    }
}