<?php

namespace Framework\Models;

use Framework\Database\Database;

class QueryBuilder {

    public $query;
    public $query_items = [
        'SELECT'    => [],
        'WHERE'     => [],
        'ORDER'     => [],
        'SET'       => [],
        'LIMIT'     => '',
        'SET'       => [],
        'INSERT'    => [],
        'DELETE'    => '',
    ];

    protected function runQuery($to_array, $get)
    {
        $query = $this->buildQuery();
        $dbh = Database::$connection->prepare($query);
        $dbh->execute();
        if (!$get) {
            return true;
        }
        if ($to_array) {
            $result = $dbh->fetchAll(\PDO::FETCH_CLASS);
            unset($result->queryString);
            echo(\json_encode($result->queryString));
            return $result;

        } else {
            $result = $dbh->fetch(\PDO::FETCH_LAZY);
            unset($result->queryString);
            echo(\json_encode($result->queryString));
            return $result;
        }
    }

    protected function buildQuery()
    {
        $select = ' SELECT ' ;
        $where  = '' ;
        $from   = ' FROM '.$this->table.' ';
        $set    = '';
        $delete = '';
        $insert = '';
        $order  = '';
        $limit  = $this->query_items['LIMIT'];
        if (empty($this->query_items['SELECT'])) {
            $select .= ' * ';
        } else {
            foreach ($this->query_items['SELECT'] as $select_items) {
                $select .= $select_items.', ';
            }
            $select = rtrim($select,", ").' ';
        }
        if (!empty($this->query_items['WHERE'])) {
            foreach ($this->query_items['WHERE'] as $where_items) {
                $where .= " ".$where_items['do']." ".$where_items['field']." ".$where_items['operator']." '".$where_items['value']."' ";
            }
        }

        if (!empty($this->query_items['SET'])) {
            $select = '';
            $from   = '';
            $set    = ' UPDATE '.$this->table.' SET ';
            foreach ($this->query_items['SET'] as $key => $value) {
                $set .= $key.' = "'.$value.'", ';
            }
            $set = rtrim($set,", ").' ';
        }

        if (!empty($this->query_items['INSERT'])) {
            $select         = '';
            $from           = '';
            $insert         = ' INSERT INTO '.$this->table.' ';
            $insert_keys    = ' ( ';
            $insert_values  = ' VALUES ( ';
            foreach ($this->query_items['INSERT'] as $key => $value) {
                $insert_keys .= $key.', ';
                $insert_values .= ' "'.$value.'", ';
            }
            $insert_keys    = rtrim($insert_keys,", ").') ';
            $insert_values  = rtrim($insert_values,", ").') ';
            $insert = $insert.$insert_keys.$insert_values;
        }

        if (!empty($this->query_items['DELETE'])) {
            $select = '';
            $delete = $this->query_items['DELETE'];
        }

        if (!empty($this->query_items['ORDER'])) {
            $order = ' ORDER BY ';
            foreach ($this->query_items['ORDER']['colums'] as $value) {
                $order .= ' `'.$value.'`, ';
            }
            $order    = rtrim($order,", ").' '.$this->query_items['ORDER']['sorted'];
        }

        $query = $select.$delete.$set.$from.$where.$limit.$insert.$order;
        return $query;
    }

    public function select($fields)
    {
        $this->query_items['SELECT'] = $fields;
        return $this;
    }

    public function first()
    {
        $this->query_items['LIMIT'] = " LIMIT 1 ";

        return $this->runQuery(false, true);
    }

    public function get()
    {
        return $this->runQuery(true, true);
    }

    public function where()
    {
        if(func_num_args() == 2) {
            $field = func_get_args()[0];
            $operator = '=';
            $value = func_get_args()[1];
        } elseif (func_num_args() == 3) {
            $field = func_get_args()[0];
            $operator = func_get_args()[1];
            $value = func_get_args()[2];
        } else {
            return false;
        }

        $items = [
            'field' => $field,
            'value' => $value,
            'operator' => $operator,
            'do' => (empty($this->query_items['WHERE'])) ? 'WHERE' : 'AND',
        ];
        array_push($this->query_items['WHERE'], $items);
        return $this;
    }

    public function update($inputs)
    {
        $this->query_items['SET'] = $inputs;
        return $this->runQuery(false, false);
    }

    public function insert($inputs)
    {
        $this->query_items['INSERT'] = $inputs;
        return $this->runQuery(false, false);
    }

    public function delete()
    {
        $this->query_items['DELETE'] = 'DELETE ';
        return $this->runQuery(false, false);
    }

    public function order($inputs, $sorted = '' )
    {
        $this->query_items['ORDER']['colums'] = $inputs ;
        $this->query_items['ORDER']['sorted'] = $sorted ;
        return $this;
    }

    public function find($id)
    {
       return $this->where("id", $id)->first();
    }

}
