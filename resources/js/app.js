"use strict";

import bar from './bar';
let a = 5;

bar();

$.ajaxSetup({
    headers: { 'FRAMEWORK': 'APPJS' }
});

$( document ).ready(function() {
    
    $('#ensureBtn').on('click', function() {
        alert('bt is clicked');
    })

    $('#send_ajax').on('click', function() {
        $.ajax({
            url: "/api/user/1",
            method: "GET",
            data: {
                message: 'message',
            }
        }).done(function (response) {
            console.log(response);
            console.log('success');
        }).fail(function (response) {
            console.log(response);
            console.log('error');
        });
    })
    document.addEventListener("DOMContentLoaded", function () {
        $('#ensureBtn').on('click', function () {
            require.ensure(["./ensure"], function(require) {
                let ensure = require("./ensure");
                ensure();
            });
        })
    });
});


