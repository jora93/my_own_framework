<?php
namespace App\Controllers;

use App\Models\User;
use Framework\Middleware\CheckRequestLoadType;
use Framework\Controller\BaseController;

class UserController extends BaseController{

    public function __construct()
    {
        $this->middleware(['check_web']);
        $this->user = new User();
    }

    public function index()
    {
        $users = $this->user->get();
        return view('users.index')->with(['users' => $users]);
    }

    public function getUser($id)
    {
        var_dump($this->user->find($id));
    }

    

}