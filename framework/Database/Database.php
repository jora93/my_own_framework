<?php

namespace Framework\Database;

Class Database {

    public static $connection = null;


    public function __construct()
    {
        $connection = new \PDO( "mysql:host=".env('DB-SERVERNAME').";dbname=".env('DB-NAME'), env('DB-USERNAME'), env('DB-PASS') );

        $connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        if (!$connection) {
            die("Connection failed");
        }
        self::$connection = $connection;
    }
}