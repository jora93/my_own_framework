<?php

namespace Framework;

use Framework\Routing\Route;
use Framework\Routing\Router;
use Framework\Request\Request;
use Framework\Response\Response;

use App\Kernel;

class Application {

    private static $providers = [
        \Framework\Database\Database::class,
        \Framework\Request\Request::class,
        \Framework\Routing\Router::class,
        \Framework\View\View::class,
    ];

    private static $_instance = null;

    private function __construct()
    {
        require(dirname(__FILE__) . "/../framework/Helpers/index.php");
        $kernel = new Kernel;
        $kernel->requireRoutes();
        $this->runProviders(self::$providers);
    }

    protected function __clone() {}

    static public function getInstance()
    {
        if(is_null(self::$_instance))
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function run()
    {
        $request = new Request();
        $this->runAction(Router::match($request), $request);
    }

    private function runAction($route, $request)
    {
        if ($route) {
            $request        = $request->makeRequest($route);
            $class          = $request->controller;
            $object         = new $class();
            $middlewares    = $object->getMiddlewares();
            $response       = true;
            if (!empty($middlewares)) {
                foreach ($middlewares as $middleware) {
                    $middleware = new $middleware;
                    if ( ( $response = $middleware->handle() ) instanceof Response) {
                        return $this->render( $response );
                    }
                }
            }
            
            $action     = $request->action;
            $response   =  $object->$action(...$request->params);
            return $this->render( $response );
        }
    }

    public function render($response)
    {
        if ($response instanceof Response) {
            return $response->render();
        }
    }

    private function runProviders($classes)
    {
        foreach ($classes as $class){
            new $class;
        }
    }

}
