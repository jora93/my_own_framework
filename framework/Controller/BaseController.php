<?php

namespace Framework\Controller;

use App\Kernel;

class BaseController
{

    protected $middlewares = [];

    protected function middleware($middlewares)
    {
        foreach ($middlewares as $middleware_name) {
            $kernel = new Kernel;
            $middleware = $kernel->getMiddleware($middleware_name);
            $this->middlewares[$middleware_name] = $middleware;
        }
    }

    public function getMiddlewares()
    {
        return $this->middlewares;
    }
}