<?php

namespace App\Providers;

use  App\Providers\AppServiceProvider;
use Framework\Routing\Route;

class RouteServiceProvider extends AppServiceProvider{

   protected $namespace = 'App\Controllers';

   public function __construct()
   {
   }

   public function map()
   {
       $this->mapWebRoutes();
       $this->mapApiRoutes();
   }

   protected function mapWebRoutes()
   {
       $route = new Route();
       $route->namespace($this->namespace)
           ->route(base_path('routes/route.php'));
   }

   protected function mapApiRoutes()
   {
       $route = new Route();
       $route->namespace($this->namespace.'\Api')
           ->route(base_path('routes/api.php'));
   }
}